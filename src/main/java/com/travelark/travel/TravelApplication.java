package com.travelark.travel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.travelark.travel.dto.Response;

@SpringBootApplication
public class TravelApplication {

	public static void main(String[] args) {		
		SpringApplication.run(TravelApplication.class, args);
	}

}

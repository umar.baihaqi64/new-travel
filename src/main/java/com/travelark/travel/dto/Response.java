package com.travelark.travel.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Response {
    private int code;
    private String pesan;
    private Object data;

}

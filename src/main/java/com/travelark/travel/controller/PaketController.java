package com.travelark.travel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.travelark.travel.dto.Response;
import com.travelark.travel.entity.Paket;
import com.travelark.travel.service.PaketService;

@RestController
@RequestMapping("paket")
public class PaketController {
    @Autowired PaketService service;
    // create
    @PostMapping
    public ResponseEntity<Response>save(@RequestBody Paket paket) {
        Response rsp = Response.builder().code(200).pesan("OKE").data(service.simpan(paket)).build();
        return new ResponseEntity<Response>(rsp,HttpStatus.CREATED);
    }
    // read
    @GetMapping("{id}")
    public ResponseEntity<Response>filter(@PathVariable Long id) {
        Response rsp = Response.builder().code(200).pesan("OKE").data(service.filter(id)).build();
        return new ResponseEntity<Response>(rsp,HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Response>list() {
        Response rsp = Response.builder().code(200).pesan("OKE").data(service.list()).build();
        return new ResponseEntity<Response>(rsp,HttpStatus.OK);
    }

    // update
    @PutMapping("{id}")
    public ResponseEntity<Response>ubah(@PathVariable Long id, @RequestBody Paket paket) {
        Response rsp = Response.builder().code(200).pesan("OKE").data(service.ubah(id,paket)).build();
        return new ResponseEntity<Response>(rsp,HttpStatus.OK);
    }

    // delete
    @DeleteMapping("{id}")
    public ResponseEntity<Response>hapus(@PathVariable Long id) {
        Response rsp = Response.builder().code(200).pesan("OKE").data(service.hapus(id)).build();
        return new ResponseEntity<Response>(rsp,HttpStatus.OK);
    }


}

package com.travelark.travel.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.travelark.travel.entity.Paket;
@Repository
public interface PaketRepository extends JpaRepository<Paket, Long> {
    public Optional<Paket> findById(Long id);
}

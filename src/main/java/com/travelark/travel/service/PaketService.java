package com.travelark.travel.service;

import java.util.List;

import com.travelark.travel.entity.Paket;

public interface PaketService {
    // Create
    public Paket simpan(Paket paket);
    // Read
    public Paket filter(Long id);
    public List<Paket> list();
    // Update
    public Paket ubah(Long id,Paket paket);

    // Delete
    public Paket hapus(Long id);
}

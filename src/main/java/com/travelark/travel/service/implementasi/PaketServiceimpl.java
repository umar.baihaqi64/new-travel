package com.travelark.travel.service.implementasi;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.travelark.travel.entity.Paket;
import com.travelark.travel.repository.PaketRepository;
import com.travelark.travel.service.PaketService;
@Service

public class PaketServiceimpl implements PaketService{

    @Autowired PaketRepository repository;
    @Override
    public Paket simpan(Paket paket) {
        return repository.save(paket);
    }

    @Override
    public Paket filter(Long id) {
        Optional<Paket> check = repository.findById(id);
        if(check.isPresent()) {
            return check.get();
        }
        return null;
    }

    @Override
    public List<Paket> list() {
        return this.repository.findAll();
    }

    @Override
    public Paket ubah(Long id, Paket paket) {
        Paket check = this.filter(id);
        if(check == null) {
            return null;
        }
            check.setNama_paket(paket.getNama_paket());
            check.setHarga(paket.getHarga());
            check.setTujuan(paket.getTujuan());
            check.setCustomer_id(paket.getCustomer_id());
            return repository.save(check);
    }

    @Override
    public Paket hapus(Long id) {
        Paket check = this.filter(id);
        if(check == null) {
            return null;
        }
        else{
            repository.delete(check);
            return check;
        }
   }    
}
